# HR Guide - Volt Nederland

**Table of contents:**

[1. Introduction](#introduction)

[2. Overview newsletter subscriber / local volunteer / member / team
member](#overview-newsletter-subscriber-local-volunteer-member-team-member)

> [2a. Newsletter subscribers / Vrienden van
> Volt](#a.-newsletter-subscribers-vrienden-van-volt)
>
> [2b. Local volunteers
> (active/non-active)](#b.-local-volunteers-activenon-active)
>
> [2c. Members](#c.-members)
>
> [2d. Active Team Members](#d.-active-team-members)

[3. Onboarding members](#onboarding-members)

> [3a. Onboarding process](#a.-onboarding-process)
>
> [3b. Onboarding steps in the
> database](#b.-onboarding-steps-in-the-database)
>
> [i. Where to find applicant
> members](#i.-where-to-find-applicant-members)
>
> [ii. Indicate in the database you (will) contact an applicant
> member](#ii.-indicate-in-the-database-you-will-contact-an-applicant-member)
>
> [iii. Use the interview process in the
> database](#iii.-use-the-interview-process-in-the-database)
>
> [FIRST: Check personal data and adjust when
> necessary](#first-check-personal-data-and-adjust-when-necessary)
>
> [SECOND: Questions page](#second-questions-page)
>
> [THIRD: Approve / Volunteer /
> Reject](#third-approve-volunteer-reject)
>
> [LASTLY: Volt NL Board confirms
> membership](#lastly-volt-nl-board-confirms-membership)
>
> [3c. Standard templates for onboarding
> members](#c.-standard-templates-for-onboarding-members)
>
> [i. Automatic email sent to applicant member (step
> 2.1)](#i.-automatic-email-sent-to-applicant-member-step-2.1)
>
> [ii. Automatic email sent to responsible local HR lead (step
> 2.2)](#ii.-automatic-email-sent-to-responsible-local-hr-lead-step-2.2)
>
> [iii. Optional First Welcome email sent by local HR lead (step
> 3)](#iii.-optional-first-welcome-email-sent-by-local-hr-lead-step-3)
>
> [iv. Optional reminder Whatsapp message (step
> 3.1)](#iv.-optional-reminder-whatsapp-message-step-3.1)
>
> [v. Optional reminder email (step
> 3.1)](#v.-optional-reminder-email-step-3.1)
>
> [vi. Confirmation email membership (step
> 6)](#vi.-confirmation-email-membership-step-6)
>
> [vii. Follow-up email with additional information to become active -
> sent by local HR
> lead](#vii.-follow-up-email-with-additional-information-to-become-active---sent-by-local-hr-lead)
>
> [3d. What does the sign-up process look like for an applicant
> member?](#d.-what-does-the-sign-up-process-look-like-for-an-applicant-member)

[4. Welcoming Local Volunteers](#welcoming-local-volunteers)

> [4a. Welcoming local volunteers:
> Process](#a.-welcoming-local-volunteers-process)
>
> [4b. Welcoming local volunteers: Steps in the
> database](#b.-welcoming-local-volunteers-steps-in-the-database)
>
> [4c. Standard templates for welcoming
> volunteers](#c.-standard-templates-for-welcoming-volunteers)

[5. General database guide](#general-database-guide)

> [5a. How to change personal details of a
> member](#a.-how-to-change-personal-details-of-a-member)
>
> [5b. How to transfer or add a member / volunteer to a (new)
> team](#b.-how-to-transfer-or-add-a-member-volunteer-to-a-new-team)
>
> [5c. How to promote a member to team
> member](#c.-how-to-promote-a-member-to-team-member)

[6. FAQs](#faqs)

> [6a. Becoming a member from being a
> volunteer](#a.-becoming-a-member-from-being-a-volunteer)
>
> [6b. Becoming a Volt Europa
> member](#b.-becoming-a-volt-europa-member)
>
> [6c Membership fee](#c-membership-fee)
>
> [6d Termination of membership](#d-termination-of-membership)

 

## 1. Introduction
-------------------

The Volt Nederland HR guide should support local HR leads and City teams
in order to manage their HR tasks such as onboarding. This document is
also meant to streamline the HR activities across the Netherlands and
assist Volt Nederland to move towards a more uniform approach when it
comes to HR. Lastly, this guide helps to develop and maintain continuity
of HR knowledge and activities within Volt Nederland.

The HR guide is mainly written for local HR leads, but also for Volters
who are trying to set up a City Team, this guide should help them
administer the HR tasks. It is the responsibility of the national HR
team to keep this up to date and involve the local HR leads in this
process to ensure local HR leads are empowered. Not only the
information, but also the processes described in this guide should be
re-evaluated, improved and updated. This is a continuous and
collaborative effort between the local HR leads and the National HR
team, in which the national HR team is ultimately responsible.

The HR Guide currently includes an overview of newsletter subscribers,
local volunteers and members; the onboarding process; welcoming of local
volunteers; basic manual of how the database works as well as FAQs,
which local HR leads may be asked.

In the future, the following topics may be included: how to elect or
appoint City Leads or other core functions; diversity within a City
Team; guidelines how we work together within Volt etc.

## Overview newsletter subscriber / local volunteer / member / team member
----------------------------------------------------------------------------------

| |**Newsletter subscriber / Vrienden van Volt** | **Local volunteer** | **Member** | **Active Team Member**|
|:-----------------------|:----------------------:|:----------------------:|:-----------:|:------------------------------------------------------------------:|
|**Legal members** | No                         |                     No           |         Yes        |      Yes|
|**Pay membership fee** | No     |                                         No         |           Yes       |       Yes |
|**Voting rights**          |          No         |                                     No   |                 After 3 months  | After 3 months |
|**Accepted statutes and rulebook**  | No         |                                     No    |                Yes    |          Yes |
|**Access to Workplace**           |   No         |                                     No    |                No     |          Yes |
|**Volt email**                    |   No         |                                     No     |               No      |         Yes |
|**Receives newsletter**           |   Yes         |                                    Yes     |              Yes      |        Yes |

### **2a. Newsletter subscribers / Vrienden van Volt**

These people follow Volt from a distance. They only receive our
newsletter and are rather passive.

### **2b. Local volunteers (active/non-active)**

Volunteers can be active and can participate in a team, but are usually
not able to empower others. They might come to meet-ups organized by
others and engage in discussions during those events, but they usually
do not lead a group or project. They can be asked to help out during
bigger events such as a Congress.

In the database Volunteers can be marked 'Active' and 'Inactive', this
function is there to easily keep an overview of your active Volunteers.
This is a pool of people the City Team can rely on when extra hands are
needed.

### **2c. Members**

Members have legal membership and therefore voting rights (except for
the first three months). Some members are rather passive and only like
to financially support Volt by paying the membership fee and might not
have time to engage in organizing activities. They might show-up every
now and then at a local event and attend congresses.

### **2d. Active Team Members**

Active Team Members have to be legal members of Volt Nederland. They
have voting rights (except the first three months), and these are the
people who form the core of our movement. They are active within the
organisation and are able to empower and coordinate others.

## 3. Onboarding members
-------------------------

Volt Nederland is part of an European inclusive political grassroots
movement Volt Europa, both are aiming to tackle today's challenges such
as climate change and the migration crisis together from an European
perspective.

Volt NL is an inclusive organisation. Within Volt NL the 'activation'
and 'empowerment' of members and volunteers are central themes.
Secondly, As multiple Volters put it: "**As Volt we do not have money,
but we have people**," we are a people powered movement, meaning that
our members and volunteers are a fundamental component to our success.

Therefore, we implemented the onboarding process in order to
**personally welcome** new members into our organisation. Personally
welcoming members supports our goals: i) **engaging** interested people
from the beginning, ii) **activating** members and ultimately
**empowering** members and iii) getting to know the interested people
and see whether their views are in line with Volt.

### **3a. Onboarding process**

### **3b. Onboarding steps in the database**

After a person applies, the Local HR lead should contact the applicant
member

within 5 days after application to welcome them and complete the
onboarding interview. The HR team advises to directly call the applicant
member. However, if you wish you can also first establish contact via
email or whatsapp.

**Please update an applicant member in the database in case you cannot
reach this person.** An applicant member can be updated in the database
(approve membership request) after you sent the reminder email to the
applicant. It would be great if this person reacts, however it is not a
problem if this does not happen. Please note: a person is only fully a
member of Volt after i) the local HR lead approved the membership
request and ii) the board confirms the membership request.

We suggest you to go through these steps when you are with the applicant
member or when you are calling as you will have to check whether the
information is correct and / or valid.

#### **i. Where to find applicant members** 

This is what the database (volt.team) looks like when you log in. You
will start in your own profile and on the left hand side there are some
menus. Click on the 'Members' menu in order to get an overview of all
the applicant members.

####  **ii. Indicate in the database you (will) contact an applicant member**
![](media/image6.png)

After clicking on the 'Members' menu, you will be redirected to this
screen. Here you see an overview of all your applicant members. In this
window you have to indicate if you will contact an applicant member, so
that other Volters know you are in the process of contacting this
specific applicant member.

![](media/image11.png)

[Please do not forget to indicate in the database that the applicant has
been contacted.]

#### **iii. Use the interview process in the database**

Once you assigned an applicant member to yourself, the interview button
will appear. After clicking on the 'interview button' you will be
redirected to the page below. It displays an overview of the general
information. Click on 'start interview' in order to start step 4 of the
onboarding process.

![figure](media/image12.png)

##### **FIRST: Check personal data and adjust when necessary**

-   Email address

-   Phone number

-   Date of birth

-   Address information

> Click next step to continue the onboarding steps in the database.

![](media/image13.png)

##### ![](media/image32.png)

##### **SECOND: Questions page**

> The questions included in this page **do not need to be asked**! The
> onboarding should be an informal introductory conversation. It should
> not turn into a formal interview or interrogation. You may use some of
> the questions in case the conversation is hard to get going.
> Ultimately, you want to

-   get an idea of who the applicant member is

-   how this person heard about Volt and why they would like to become a
    > member

-   get an idea whether the views of the applicant member fit with
    > Volt's vision and goals

-   whether and how this person would like to become active within Volt.

![](media/image33.png)

> *Please note: you will not be able to see these answers after you
> finish the interview in the database.*

##### **THIRD: Approve / Volunteer / Reject**

> At the end of the interview the local HR lead decides to

-   Approve the membership request: applicant fits within Volt and wants
    > to become a legal member

-   Turn the membership request into a volunteer request: applicant
    > indicates that s/he would like to be a volunteer for now /
    > Applicant wishes to withdraw membership application

-   [Reject the membership application](#8dluq7arjiv): Applicant
    > does not fit within Volt, because their values do not match Volt's

> ![](media/image3.png)

Rejection of membership request

-   Person does not fit within Volt

-   Person is a member of another political party of which the vision
    > and goals are not in line with the vision of Volt.

##### **LASTLY: Volt NL Board confirms membership**

> Only the co-presidents of Volt Nederland and the board member support
> can view this option in the database. The above mentioned board
> members confirm the membership request after which the process has
> been completed.
>
> ![](media/image29.png)

### **3c. Standard templates for onboarding members**

This section facilitates local HR leads by displaying the **emails,
which are automatically send** as well as **templates** for
communication. [The texts below are templates and therefore only
guidelines. Local HR leads can always change the texts to the
context.]

#### **i. Automatic email sent to applicant member (step 2.1)**

> *Beste \[Name applicant member\],*
>
> *Wat fijn dat je je aangemeld hebt als lid van Volt!*
>
> *Een jonge, progressieve beweging voor een daadkrachtig en humaan
> Europa.*

*Ons team neemt zo snel mogelijk contact met je op.*

> *Wil je ook actief bijdragen aan je lokale Volt team?*
>
> *Of heb je gewoon zin in een leuk gesprek met Volters uit de buurt?*
>
> *Kijk hier waar we allemaal zitten.
> [https://www.voltnederland.org/steden](https://webmail.campus.leidenuniv.nl/owa/redir.aspx?C=WFCgm2i0wXxALfXa367Y7WV1a2Txfbp6IHse_Sfyww_ZS9Wc1XDXCA..&URL=https%3a%2f%2fwww.voltnederland.org%2fsteden)*
>
> *Mocht je ook nog een financieel steentje aan onze campagne bij willen
> dragen*
>
> *kan dat via onze crowdfunding.
> [https://www.voltnederland.org/doneer](https://webmail.campus.leidenuniv.nl/owa/redir.aspx?C=YdJysfTTfndxzIaIqzUgB8FrbAAuyEHmQlELbWkOl1HZS9Wc1XDXCA..&URL=https%3a%2f%2fwww.voltnederland.org%2fdoneer)*
>
> *We zijn je heel dankbaar voor elke donatie!*
>
> *En vergeet ons niet te volgen op social media:*
>
> *Twitter:
> [https://twitter.com/voltnederland](https://webmail.campus.leidenuniv.nl/owa/redir.aspx?C=7kmdUcJBDmv5Z9WC57QycF9XoU5ReiIZ_sQeAoQnZxrZS9Wc1XDXCA..&URL=https%3a%2f%2ftwitter.com%2fvoltnederland)*
>
> *YouTube:
> [https://www.youtube.com/c/voltnederland](https://webmail.campus.leidenuniv.nl/owa/redir.aspx?C=ZtnapvW8dOm77BpUr4QtbsqTUlsD60LvgrFNOHwYN8zZS9Wc1XDXCA..&URL=https%3a%2f%2fwww.youtube.com%2fc%2fvoltnederland)*
>
> *Instagram:
> [https://www.instagram.com/voltnederland/](https://webmail.campus.leidenuniv.nl/owa/redir.aspx?C=OQ1SERUO5SMT0bXDeQBpxdvu6dZdz44rJb_vFIXAH6bZS9Wc1XDXCA..&URL=https%3a%2f%2fwww.instagram.com%2fvoltnederland%2f)*
>
> *Facebook:
> [https://www.facebook.com/VoltNederland/](https://webmail.campus.leidenuniv.nl/owa/redir.aspx?C=oV3my6hZhlniNI-vojRC8qunsXS8sNntHp7ooQQ2E17ZS9Wc1XDXCA..&URL=https%3a%2f%2fwww.facebook.com%2fVoltNederland%2f)*
>
> *Groet,*
>
> *\[Name local HR lead\] van Volt Europa*
>
> *\-\--*
>
> *Volt Europa A.S.B.L.*
>
> *Banzelt 4A,*
>
> *6921 Roodt-sur-Syre*
>
> *Luxembourg*
>
> *Registration Number: F11591*
>
> [*https://www.volteuropa.org*](https://webmail.campus.leidenuniv.nl/owa/redir.aspx?C=hElAfcT_tWPODM-AgeV5bJLL7xRR61ZK_SlDIzGtXwnZS9Wc1XDXCA..&URL=https%3a%2f%2fwww.volteuropa.org)

#### **ii. Automatic email sent to responsible local HR lead (step 2.2)**

*Hi \[Name(s) local HR lead(s)\],*

> ***You have a new member application in your team, \[Name applicant
> member\] from \[City\], Netherlands.***

*You can see their full profile here
[https://volt.team/members/test](https://volt.team/members/test)*

*Please contact them and invite them to a team event in coming days!*

*Vasiliki*

*for the Community Team*

*\--*

*Vasiliki Panayotopulu*

*European Community Team*

> *P.S.: This is an automated message. If you think you shouldn't be
> receiving this email, please ask your team lead or HR lead to check
> your status in volt.team.*

*Volt Europa*

*[Facebook](https://www.facebook.com/VoltEuropa/) \|
[Twitter](https://twitter.com/Volteuropa) \|
[LinkedIn](https://www.linkedin.com/company/volteuropa/) \|
[Website](http://www.volteuropa.org/)*

#### **iii. Optional First Welcome email sent by local HR lead (step 3)**

> As stated before the national HR team encourages local HR leads to
> establish contact by a phone call. However, if you feel more
> comfortable sending an email, you may use the template below.
>
> ***Onderwerp**: Aanmelding bij Volt Nederland - Registration Volt
> Nederland*
>
> *Beste \[naam aanmelder\],*
>
> *Bedankt voor je interesse in Volt Nederland!*
>
> *We kijken ernaar uit om kennis met je te maken en hopen je te
> enthousiasmeren om (actief) lid te worden van Volt Nederland. Wil jij
> bijdragen aan een betere Nederlandse en Europese samenleving of alleen
> even kennismaken? Reageer dan even op deze e-mail en laat ons weten
> wat jouw verwachtingen zijn. Wij nemen dan binnen 5 dagen telefonisch
> contact met je op!*
>
> *We hopen je snel te spreken of te ontmoeten op een van onze
> evenementen. Indien je nog vragen hebt, reageer dan gerust op deze
> e-mail.*
>
> *Hartelijke groeten,*
>
> *\[Naam Local HR Lead\]*
>
> *=====English version:=====*
>
> *Dear \[Name applicant\],*
>
> *Thanks for expressing an interest in Volt Nederland!*
>
> *We look forward to getting to know you and hope we can enthuse you to
> become a (active) member of Volt Nederland. Are you eager to
> contribute to a better Dutch and European society or just looking
> forward to get acquainted? Please respond to this mail and let us know
> what your expectations are. We shall then contact you by phone within
> 5 days!*
>
> *We hope to speak to you soon or to meet you in person at one of our
> events. If you have any questions, please respond to this email.*
>
> *Yours sincerely,*
>
> *\[Name local HR lead\]*

#### **iv. Optional reminder Whatsapp message (step 3.1)**

> In case an applicant member does not respond, you can either send them
> a reminder via whatsapp and use this template or send an email and use
> template v.
>
> *Ha \[naam aanmelder\],*
>
> *Een tijdje geleden heb je lidmaatschap aangevraagd bij Volt
> Nederland. Ik heb eerder geprobeerd contact met je op te nemen, maar
> dat is tot nu toe helaas nog niet gelukt. Ik vroeg me af of je
> wellicht interesse hebt om even kort te bellen?*
>
> *Indien ik geen reactie van je ontvang zal ik je aanmelding
> bevestigen. Je bent dan lid van Volt Nederland. Je zal wekelijks onze
> nieuwsbrief ontvangen en je hebt dan na drie maanden stemrecht tijdens
> ons Congressen. Natuurlijk ben je altijd welkom om actief te worden
> binnen Volt Nederland. Je kan dan reageren op dit appje of je kan een
> email sturen naar:
> [info\@voltnederland.org](mailto:info@voltnederland.org).*
>
> *Hartelijke groeten,*
>
> *\[Naam local HR lead\]*
>
> *=== English version ===*
>
> *Hi \[name applicant\],*
>
> *A while ago you requested legal membership of Volt Nederland. Earlier
> I tried to contact you, but so far I have not been able to reach you.
> I wondered if you are interested to have a short phone call?*
>
> *If I do not receive a response, I shall confirm your membership
> request. Then you will be a member of Volt Nederland. You will receive
> our newsletter every week and after 3 months you will have voting
> rights during our Congresses. Of course, you are always welcome to
> become active within Volt Nederland. All you then need to do is reply
> to this Whatsapp message or send a mail to:
> [info\@voltnederland.org](mailto:info@voltnederland.org).*
>
> *Best,*
>
> *\[Local HR lead\]*

#### **v. Optional reminder email (step 3.1)**

> In case an applicant member does not respond, you can either send this
> reminder email or send a whatsapp message and use template iv.
>
> ***Onderwerp**: Bevestiging Lidmaatschap - Confirmation Lidmaatschap*
>
> *Beste \[naam aanmelder\],*
>
> *Een tijd geleden heb je je aangemeld bij Volt Nederland via onze
> website. We hebben je erna meerdere keren proberen te bereiken om je
> op een warme manier te verwelkomen binnen onze organisatie en kennis
> met je te maken. Helaas is het ons niet gelukt om met je in contact te
> komen.*
>
> *We zullen daarom je aanmelding bevestigen. Je bent nu lid van Volt
> Nederland. Dit houdt in dat je regelmatig onze nieuwsbrief zal
> ontvangen en dat je over drie maanden stemrecht hebt tijdens onze
> Congressen. Wil je actief worden binnen Volt Nederland, dan kan dat
> natuurlijk altijd. Stuur dan een email naar:
> [info\@voltnederland.org](mailto:info@voltnederland.org).*
>
> *We hopen je zo voldoende geïnformeerd te hebben. Laat het ons gerust
> weten indien je verdere vragen hebt.*
>
> *Hartelijke groeten,*
>
> *\[Local HR lead\]*
>
> *=== English version ===*
>
> *Dear \[name applicant\],*
>
> *A while ago you requested legal membership with Volt Nederland via
> our website. We tried to contact you a couple of times in order to
> warmly welcome you into our organisation and to get acquainted.
> Unfortunately, we were not able to reach you.*
>
> *We will therefore confirm your membership request. You will then be a
> member of Volt Nederland. You will receive our newsletter every week
> and after three months you will have voting rights during our
> Congresses. You are always welcome to become active within Volt
> Nederland. All you need to do is send a mail to:
> [info\@voltnederland.org](mailto:info@voltnederland.org).*
>
> *We hope to have informed you sufficiently. Please do not hesitate to
> contact us in case you have any questions.*
>
> *Best,*
>
> *\[Local HR lead\]*

#### **vi. Confirmation email membership (step 6)**

> ***Onderwerp**: Lidmaatschap Volt Nederland bevestigd - Volt Nederland
> Membership confirmed*
>
> *Beste \[naam lid\]*
>
> *Welkom bij Volt Nederland.*
>
> *Tijdens het laatste Volt Nederland bestuursoverleg is jouw aanvraag
> voor lidmaatschap besproken, en jouw lidmaatschap bij Volt Nederland
> is nu bevestigd, zoals omschreven in Art. 3 van de statuten van Volt
> Nederland.*
>
> *Je hebt hierbij alle rechten en plichten verkregen van volledig
> lidmaatschap van Volt Nederland, behalve stemrecht tijdens de Volt
> Nederland Congressen dit recht krijg je na 3 maanden lidmaatschap.*
>
> *We hopen je zo voldoende geïnformeerd te hebben. Laat het ons gerust
> weten indien je verdere vragen hebt.*
>
> *Hartelijke groeten,*
>
> *\[Local HR lead\]*
>
> *=====English version:=====*
>
> *Dear \[name member\]*
>
> *Welcome to Volt Nederland!*
>
> *During a recent Volt Nederland board meeting,* *your application for
> membership has been confirmed. You are now an individual Member of
> Volt Nederland as described in Art. 3 of the statutes of Volt
> Nederland.*
>
> *You have all the rights and duties of a member of Volt Nederland,
> except for voting rights during the Volt Nederland Congresses as you
> need to be a member for at least 3 months for this.*
>
> *We hope to have informed you sufficiently. Please do not hesitate to
> contact us in case you have any questions.*
>
> *Best,*
>
> *\[Local HR lead\]*

#### **vii. Follow-up email with additional information to become active - sent by local HR lead**

> In order to ensure the new member receives the necessary tools in
> order to become active within our organisation, the local HR leads
> sends a follow-up email with the following information:

-   Personal welcome

-   What's next

-   How we work \[in our City Team\]

    -   Structure of team, contacts etc.

    -   Meetings & Events

-   Internal Comms

```
<!-- -->
```
-   Additional information & links Volt EUR & NL

Here below you can find an (extensive) example:

> **Onderwerp:**
>
> *"Beste ....*
>
> *Fijn om zojuist met je te spreken en elkaar een beetje te leren
> kennen. Graag wil ik je nogmaals hartelijk bedanken voor je aanmelding
> bij Volt. In onderstaande tekst vind je handige informatie en links
> die je kunnen helpen om actief te worden binnen Volt. Daarnaast willen
> we je graag op de hoogte houden via onze nieuwsbrief en Whatsapp groep
> en hopen we binnenkort persoonlijk met je kennis te maken op een van
> onze bijeenkomsten.*
>
> ***Wat nu?***
>
> *Nu je officieel lid bent geworden, willen we je graag uitnodigen om
> actief te worden. De makkelijkste manier om dat te doen is via
> persoonlijk contact. Hieronder vind je informatie over bijeenkomsten
> en speciale gebeurtenissen. Nadat we persoonlijk kennis hebben gemaakt
> en je actief bent geworden, zullen we een Volt Europa e-mailaccount
> voor je aanmaken. Daarmee heb je toegang tot ons interne digitale
> platform Workplace, dat alle nationale afdelingen met elkaar
> verbindt.*
>
> ***Het Lokale team -- hoe gaan we te werk.\
> **Op lokaal niveau is Volt XXX is georganiseerd in teams: YYY. Deze
> teams komen geregeld bijeen. Daarnaast komen de team leads eens in de
> twee weken samen om de algemene voortgang van het city team te
> coördineren.\
> We zijn altijd op zoek naar nieuwe actieve teamleden, dus aarzel
> vooral niet en sluit je aan! We zullen je dan laten weten wanneer en
> waar een bepaald team bijeen komt.\
> Naast deze teambijeenkomsten, zijn er ook geregeld evenementen die
> open zijn voor alle leden. Meer informatie hierover kan je via deze
> link ZZZ vinden.*
>
> ***Interne Communicatie\
> **Intern communiceren we via onze lokale nieuwsbrief en whatsapp
> groep(en). Op landelijk niveau hebben we ook zulk soort
> communicatiekanalen. Als je interesse hebt om deze ook te volgen, kun
> je dit via deze links regelen:*

-   *Volt NL Info: XXX*

-   *Volt NL Dialogue Forum: XXX\
    > \
    > We zullen jouw e-mail gebruiken om belangrijk informatie te delen
    > over bijvoorbeeld de Algemene Vergaderingen op NL en EU niveau en
    > mogelijkheden om mee te stemmen. Het is als Pan-Europese politieke
    > beweging cruciaal dat alle leden hun mening kunnen uiten m.b.t.
    > kerngebieden en -posities. We hebben daarvoor een online
    > stemsysteem voor Europese stemmingen. Het is dus belangrijk dat we
    > een actief e-mailadres van je hebben, zodat je kan meestemmen
    > tijdens onze Europese besluitvorming.*

> ***Aanvullende informatie\
> **Hoewel we je adviseren eerst actief te worden op lokaal niveau, is
> het natuurlijk ook mogelijk om actief te worden op nationaal of
> Europees niveau. Als je hierin geïnteresseerd bent, kan je landelijke
> vacatures op onze
> [website](https://www.voltnederland.org/internal_vacancies)
> bekijken. Ook kunnen we de opties verder bespreken in een gesprek. Ons
> doel is om jou te helpen je plek te vinden binnen Volt!*
>
> *Indien je vragen hebt, neem dan gerust contact op met mij of met een
> van de andere team leads die hierboven vermeld staan. We kijken er
> naar uit om je binnenkort persoonlijk te ontmoeten!*
>
> *Hartelijke groeten,*
>
> *(lokale HR lead of lid)*
>
> *=====English version:=====*
>
> *Dear ...,*
>
> *On behalf of Volt Europa, I would like to give you a warm welcome.
> It's wonderful to have you on board! You are now a legal member of
> Volt Nederland and Volt Europe. You have taken a step towards changing
> the way politics is done in Europe. A step to strengthen the Europe we
> all share. Thank you!*
>
> ***What\'s next?**\
> Now that you are a legal member, we also like to invite you to become
> active, and the easiest way is to do this is to connect in person.
> Below you will find information about meetings and special events.
> Once we have the opportunity to meet you in person and you became an
> active member, we can provide you with a Volt Europa email account and
> with that access to our internal platform Workplace, which connects
> our Volt chapters across Europe.*
>
> ***Local Team - How We Work**\
> On the local level, Volt XXX is structured into teams: YYY.* *These
> teams meet regularly by themselves, and the team leads get together
> every two weeks to coordinate the overall progress of the city team.
> As we are always looking for new active team members, please do not
> hesitate to reach out and join one of them! We will then let you know
> when and where the specific team is meeting.*
>
> *Besides these team meetings, we also have regular moments which are
> open for all members. See this link ZZZ for detailed information.*
>
> ***Internal Communication**\
> Internally we mainly communicate through our local newsletter and our
> Whatsapp groups. On the national level, we have similar communication
> channels. If you are interested in joining these as well, you can do
> so through these links:*

-   *Volt NL Info: XXX*

-   *Volt NL Dialogue Forum: XXX*

> *Please note that we will also use your email to share important
> information about our **General Assemblies** at NL and EU level and
> **Voting** opportunities. As a Pan-European political movement it is
> crucial that each member is able to express their opinion on key areas
> and positions. We have a great electronic voting system for that. So
> please make sure that we have your most accurate email in order for
> you to have a voice in our movement. *
>
> ***Additional Information**\
> While we do prefer you becoming active first on the local level, it is
> also possible to work in the National or European teams. If you are
> interested in these, you can already look at our vacancies on our
> [website](https://www.voltnederland.org/internal_vacancies),
> and we can discuss the options. Our aim is to help you find your ideal
> place in Volt, based on your preferences and skills!*
>
> *If you have questions don't hesitate to contact me or any of the
> other team leads mentioned above- and we're very much looking forward
> to meeting you soon!*
>
> *Best wishes, *
>
> *(local HR lead or member)*

### **3d. What does the sign-up process look like for an applicant member?**

1.  >The interested person goes to
    > [volt.team/join](https://volt.team/join)
    > ![](media/image7.png)

2.  >(S)He chooses the desired role (Member / Volunteer)
    >![](media/image30.png)

3.  >The applicant members enters her/his contact details
    > ![](media/image1.png)

4.  >(S)He enters the preferred language and her/his nationality
    > ![](media/image20.png)

5.  >The applicant member enters her/his address
    >![](media/image22.png)

6.  >(S)He enters her/his Personal information
    >![](media/image10.png)
    >![](media/image26.png)

7.  >The applicant members enters their bank account details, ticks all
    > applicable boxes and submits the membership request.
    >![](media/image8.png)
    >![](media/image28.png)

## 4. Welcoming Local Volunteers
---------------------------------

As the local HR lead or as the group responsible for HR within a City
Team it is important to empower and active volunteers as Volt Nederland
depends on human power.

### **4a. Welcoming local volunteers: Process**

For smaller City Teams (5 - 10 active core members) we advise to active
volunteers as soon as possible and see if there is an option to empower
them to become part of the core team and empower them to empower others.

The process goes as follows:

In case a bigger City Team (\> 10 active core members) struggles to
manage the membership applications, the focus is more on welcoming and
activating members rather than volunteers. However, it is always good to
try and engage the volunteers as much as you can.

### **4b. Welcoming local volunteers: Steps in the database**

Local volunteer applicants are listed on the **Volunteers** page. All
applicant volunteers are shown under the tab: 'New'

Indicate you contacted a volunteer and that you are therefore
responsible for that volunteer, by clicking on the button 'Assign to me'
on the left hand side of the
screen![](media/image31.png)

Once you click on 'Assign me' the applicant volunteer will move from the
'New' tab, to the 'Pending activation'
tab.![](media/image18.png)

You can mark a Volunteer as active or inactive in the Pending Activation
tab. If you mark them as active, they will appear under the tab
'Active'. If you mark them as inactive, they will appear under the tab
'Inactive'.

You can always change the status of a Volunteer again. You can either to
that by going to the tabs 'Active' and 'Inactive' or by going to the
specific profile of a Volunteer.

**Option 1: change status of volunteer on the tab 'Active'**

On the right hand side it is possible to change an Active Volunteer into
an Inactive
Volunteer.![](media/image27.png)

You can also change an Inactive Volunteer into an Active one by going to
the 'Inactive' tab. Click on the button 'Mark as active' on the left
hand side of the screen.

**Option 2: change the status of a Volunteer on the profile page**

In order to change the status of a volunteer on the profile page, click
on the button 'Mark as inactive' or 'Mark as active', depending on the
current status of the volunteer. The button can be found at the top of
the page.![](media/image5.png)

### **4c. Standard templates for welcoming volunteers**

1.  **[Automatic email sent to applicant volunteer]**

*Beste \[first name volunteer\],*

*Wat fijn dat je je aangemeld hebt als vrijwilliger voor Volt!*

*Een jonge, progressieve beweging voor een daadkrachtig en humaan
Europa.*

*Ons team neemt zo snel mogelijk contact met je op.*

*Mocht je ook nog een financieel steentje aan onze campagne bij willen
dragen*

*kan dat via onze crowdfunding.
[https://www.voltnederland.org/doneer](https://www.voltnederland.org/doneer)*

*We zijn je heel dankbaar voor elke donatie!*

*Je kunt natuurlijk ook lid worden voor maar € 2,50 per maand.*

[*[http://voltnederland.org/word_lid]*](http://voltnederland.org/word_lid)

*En vergeet ons niet te volgen op social media:*

*Twitter:
[https://twitter.com/voltnederland](https://twitter.com/voltnederland)*

*YouTube:
[https://www.youtube.com/c/voltnederland](https://www.youtube.com/c/voltnederland)*

*Instagram:
[https://www.instagram.com/voltnederland/](https://www.instagram.com/voltnederland/)*

*Facebook:
[https://www.facebook.com/VoltNederland/](https://www.facebook.com/VoltNederland/)*

*Groet,*

*\[Name Local HR Lead\] van Volt Europa*

*\-\--*

*Volt Europa A.S.B.L.*

*Banzelt 4A,*

*6921 Roodt-sur-Syre*

*Luxembourg*

*Registration Number: F11591*

[https://www.volteuropa.org](https://www.volteuropa.org/)

2.  **[Automatic email sent to responsible local HR lead]**

*Hi \[Names of local HR leads),*

***You have a new volunteer in your team, \[name Volunteer\] from
\[City\], Netherlands.***

*You can see their full profile here
[https://volt.team/members/test](https://volt.team/members/test)*

*Please contact them and invite them to a team event in coming days!*

*Vasiliki*

*for the Community Team*

*\--*

*Vasiliki Panayotopulu*

*European Community Team*

> *P.S.: This is an automated message. If you think you shouldn't be
> receiving this email, please ask your team lead or HR lead to check
> your status in volt.team.*

*Volt Europa*

*[Facebook](https://www.facebook.com/VoltEuropa/) \|
[Twitter](https://twitter.com/Volteuropa) \|
[LinkedIn](https://www.linkedin.com/company/volteuropa/) \|
[Website](http://www.volteuropa.org/)*

## 5. General database guide
-----------------------------

As local HR lead, you have special access to our Volt database in order
to edit details and statuses of Volt members and volunteers. Below you
can see how you can process these changes:

### **5a. How to change personal details of a member**

> Search for the respective member in the active member list or type in
> the name in the upper search box

![](media/image19.png)

> Click on the name of the respective member in the list, you will be
> redirected to their profile. Click on the "Edit member" button in the
> right-upper
> corner.

![](media/image17.png)

![](media/image24.png)

> Change details as needed and click "Update member" at the bottom of
> the page. All set.

![](media/image15.png)![](media/image16.png)

###  

### ![](media/image9.png)

###  

### **5b. How to transfer or add a member / volunteer to a (new) team**

> Search for the respective member in the active member list or type in
> the name in the upper search box, click on the name and click "edit
> member" (see also above).
>
> Go to the tab "Team Assignment"

![](media/image23.png)

###  ![](media/image21.png)

### **5c. How to promote a member to team member**

> Go to the profile of the respective member and click on the white
> button: 'More actions' and select 'Promote to Team Member'

![](media/image25.png)

> You will be redirected to the screen below. Here you will create a
> Volt email address. We always use:
> [firstname.lastname\@volteuropa.org](mailto:firstname.lastname@volteuropa.org).
> After you checked the email address, click on 'Promote'

![](media/image38.png)

> After you click promote, the member in question and yourself will
> receive automatic emails in which the member receives instructions on
> how to activate their Volt email and Workplace account.

1.  **Standard confirmation email**

> *Hello \[Name member\],*
>
> *Welcome to the Volt Team! The following email will help you
> understand a) where we are heading, b) where you can help, and c) how
> we will work together.*
>
> ***Your teams:***

-   ***Local team assignment**: You will be assigned to the \[Name City
    > Team\] , lead by \[Name City Lead\] (cc).*

> ***The first 4 steps to become a true Volter!***

1.  *Join Workplace at
    > [volteuropa.facebook.com](https://volteuropa.facebook.com/),
    > our main collaboration platform, which you also enter with your
    > Volt email address. Workplace is where you are able to meet
    > hundreds of Volters from all-over Europe. Once you are in, please
    > briefly introduce yourself on the EUR General group and join the
    > relevant groups for you! Every group has a geographical
    > caractherization first (EUR, IT, DE, FR) and then a functional one
    > (Events, Communication, etc.)*

2.  *Check out [Google
    > Drive](https://drive.google.com/drive/u/0/folders/0AMTK83WiQ5nfUk9PVA)
    > to see where we store files and collaborate on documents. You will
    > find some really useful information on our movement!*

3.  *Make sure to also check out our shared [Google
    > Calendar](https://calendar.google.com/calendar/r) to learn
    > about upcoming events and calls. Try to jump into the next call of
    > your team - it's always useful to start to get to know each
    > other!*

> ***If you have technical issues please reach out to me directly**. I
> am your first point of contact, and I am here to help! Please also
> remember that all our communication is handled via our internal
> communication tools - please check it regularly.*
>
> *We are delighted by your enthusiasm and courage. Let's rock this!*
>
> *All the best,*
>
> *\[Name local HR lead\]*

## 6. FAQs
-----------

There are certain questions which are frequently asked to local HR lead.
See below the four most asked questions.

### **6a. Becoming a member from being a volunteer**

> If someone is already in our database as a Volunteer, this person
> cannot sign-up as a member. This person can 'update' their profile by
> going through the following steps:
>
> 1.Go to [volt.team/join](https://volt.team/join) and insert the
> email address you used to sign-up as a volunteer

![](media/image14.png)

a.  **You will receive an automatic email with a personal link**

> *Dear Test,*
>
> *You (or possibly someone else) has just requested to update your Volt
> profile.*
>
> *You can update your Volt profile via the following link:*
>
> [*[https://volt.team/join/steps/target_role_selection_testlink]*](https://volt.team/join/steps/target_role_selection_testlink)
>
> *This link expires in bijna 50 jaar (2019-12-01 10:40).*
>
> *If you haven't requested this email, you can simply ignore this
> message.*
>
> *Best,*
>
> *Your Team at Volt Europa*
>
> *\-\--*
>
> *Volt Europa A.S.B.L.*
>
> *Banzelt 4A,*
>
> *6921 Roodt-sur-Syre*
>
> *Luxembourg*
>
> *Registration Number: F11591*
>
> [*[https://www.volteuropa.org]*](https://www.volteuropa.org/)

b.  Once you click on the link you will be redirected to the sign-up
    > form to become a member. Please add the missing information and
    > check if the filled out information is still correct. Accept all
    > the conditions and click on 'Meld je aan om lid te worden van
    > Volt'.

The membership request can be found under 'Members' under the tab 'New
applications'. The local HR lead receives a notification via email and
has to approve the membership request. Please view section 1b for the
required steps in the database.

After the local HR lead approved the membership request, the Board has
to confirm the membership. After this the person becomes a member of
Volt Nederland and Volt Europa.

### **6b. Becoming a Volt Europa member**

All members who signed up after September 2019 are automatically a
member of both Volt Nederland and Volt Europa.

If you have a member who became a member before September 2019, then
they have to go through the following steps:

a.  Go to [volt.team/join](https://volt.team/join) and insert the
    > email address you used to sign-up as a

> member of Volt Nederland

b.  You will receive an email with a special personal link

c.  Once you click on the link you will be redirected to the sign-up
    > form. Check if your personal details are up to date and accept the
    > statues of Volt Europa.

![](media/image14.png)

The board of Volt Europa will confirm your Volt Europa membership during
their next board meeting. You will receive a confirmation email after
the board confirmed your membership.

### **6c Membership fee**

Members pay at least €2,50 per month (€30 annually), but everyone is of
course welcome to pay maar iedereen is welkom om een hoger bedrag te
betalen. De contributie wordt voor de resterende maanden van het jaar
berekend.

### **6d Termination of membership**

**Article 4.2 Volt Nederland Statutes**

A member can terminate the membership in writing, addressed to the
executive committee of Volt Nederland. The termination will be effective
immediately upon receipt thereof by the executive committee. The member
will still owe the membership fee for the current membership fee period.
A member can also terminate his/her membership with immediate effect
within one month after he/she has become aware or has been informed of a
decision by which his/her rights are limited or his/her obligations in
respect of Volt Nederland are increased. In that case, the decision will
not apply to the relevant member. A member may not terminate his/her
membership to rule out a decision in his/her respect that changes
his/her monetary rights and obligations.

Only [board members and Team Admin] are able to terminate
memberships. Local HR leads do not have the rights in volt.team to do
so.

List of 10 core Volt values:

1.  Wij geloven dat er een transnationale pro-europese politieke
    > beweging (en dus niet alleen een partij) nodig is in Europa.

2.  Hierin speelt burgerparticipatie een belangrijke rol om de
    > menselijke waardigheid, mensenrechten, solidariteit, democratie,
    > gelijkheid voor de wet, vrijheid en de rechtsstaat binnen en
    > buiten Europa te beschermen.

3.  Wij staan voor progressief beleid om het algemeen welzijn van alle
    > inwoners van Europa te beschermen.

4.  De Europese Unie, het politieke en sociale project van onze
    > grootouders en ouders, wordt tegengehouden door gemis van
    > competentie en het niet afstemmen van gedeeld belangen. Wij willen
    > hier op inzetten om Europa te verberen.

5.  We stellen ons een staat voor die garant staat voor de rechten van
    > elk individu, de mogelijkheid voor jonge en kwetsbare individuen
    > om volledig deel te nemen aan de samenleving en een
    > solidariteitssysteem waarmee we zorgen voor een minimum
    > fatsoenlijke levensstandaard voor iedereen.

6.  Staat interventie een middel in plaats van een doel op zichzelf,
    > zijn wij van mening dat de staat zo weinig en zo snel mogelijk
    > moet ingrijpen, en zo veel en zo lang als nodig is.

7.  We stellen ons een vrije en open markteconomie voor, binnen de
    > regels van een functionerend rechtssysteem en met gelijke kansen
    > om deel te nemen en te gedijen, die de grootst mogelijke rijkdom
    > voor iedereen creëert.

8.  Wij zijn van mening dat de staat geen innovaties kan plannen of
    > voorzien, maar dat de staat ruimte moet creëren voor innovatie en
    > onderzoek mogelijk moet maken. Wij geloven dat het markteconomie
    > systemisch onevenwichtigheden creëert, die moeten worden
    > gecompenseerd.

9.  Wij geloven dat het recht op een fatsoenlijk leven ook de
    > mogelijkheid tot vrijetijdsbesteding omvat. Wij zijn van mening
    > dat we alle beroepen moeten waarderen, vooral die welke onze
    > samenlevingen het meest ten goede komen, zoals onderwijs, zorg en
    > onderzoek.

10. Wij geloven dat alleen door een brede acceptatie van Europese
    > waarden ons continent alleen kan vermijden terug te vallen in het
    > conflict dat eeuwenlang Europa doorkruiste, maar ook onze
    > samenleving helpt naar een duurzame toekomst van vrede, gedeelde
    > welvaart en internationale relevantie.
